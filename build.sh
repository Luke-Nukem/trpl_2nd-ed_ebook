#! /bin/sh

RUN="cargo run --"

# ln -s ./book/first-edition/src/img ./img
# $RUN --prefix=trpl_1st-ed --source=./book/first-edition/src --meta=./trpl_meta.yml && \
# $RUN --prefix=nomicon --source=nomicon --meta=nomicon_meta.yml && \
rm ./img
ln -s ./book/src/img ./img
$RUN --prefix=trpl --source=./book/src/ --meta=./trpl-meta.yml
rm ./img
ln -s ./book/2018-edition/src/img ./img
$RUN --prefix=trpl-2018-ed --source=./book/src/2018-edition --meta=./trpl_2018-meta.yml
rm ./img
ln -s ./book/nostarch/src/img ./img
$RUN --prefix=trpl-nostarch-ed --source=./book/src/nostarch --meta=./trpl_nostarch-meta.yml
