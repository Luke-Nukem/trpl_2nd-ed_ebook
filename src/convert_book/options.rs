pub const RELEASE_DATE: &'static str = "2019-04-21";

pub const BASE: &'static str = "markdown+smart+grid_tables+pipe_tables-simple_tables+raw_html+implicit_figures+footnotes+intraword_underscores+auto_identifiers-inline_code_attributes";

pub const EPUB: &'static str = "--standalone --self-contained --highlight-style=tango --css=lib/epub.css --table-of-contents --epub-chapter-level=2";

pub const LATEX: &'static str = "--standalone --self-contained --highlight-style=tango --top-level-division=chapter --table-of-contents --template=lib/template.tex --pdf-engine=xelatex --to=latex";
